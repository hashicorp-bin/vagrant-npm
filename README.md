# vagrant-npm
*An NPM executable package for HashiCorp's Vagrant.*

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier) [![vagrant: v0.11.7](https://img.shields.io/badge/vagrant-v0.11.7.0-6253f4.svg)](https://www.vagrant.io) [![npm downloads](https://img.shields.io/npm/dt/vagrant-npm.svg?maxAge=3600)](https://www.npmjs.com/package/vagrant-npm) [![Travis CI](https://img.shields.io/travis/steven-xie/vagrant-npm.svg)](https://travis-ci.org/steven-xie/vagrant-npm) [![CodeClimate: Maintainability](https://api.codeclimate.com/v1/badges/a1b6ec5bf81af570f5bc/maintainability)](https://codeclimate.com/github/steven-xie/vagrant-npm/maintainability)

### Preamble
I assembled [Vagrant](https://vagrant.io) into an NPM package in order for me to include it in other projects that depended on the executable. I wanted to be able to publish NPM modules with scripts like this:
```json
{
  "scripts": {
    "plan": "vagrant plan -out=my-tfplan"
  }
}
```
But without having to worry about asking users to download Vagrant externally.

---

### Installation
To use *Vagrant* as an NPM package, include it in your `package.json` dependencies:
```bash
# If you're using Yarn (recommended):
yarn add vagrant-npm

# If you're using NPM:
npm i vagrant-npm
```

Or, if you want a one-time installation that you can run arbitrarily, install it globally:
```bash
# If you're using Yarn (recommended):
yarn global add vagrant-npm

# If you're using NPM:
npm i -g vagrant-npm
```


## Usage
#### As a project dependency:
This package cannot currently be used as a typical Node module, as it does not export any entry points; it only symlinks a binary. So, the recommended use case is to use it in your `package.json` scripts:
```json
{
    "scripts": {
        "plan": "vagrant plan -out=my-tfplan",
        "apply": "vagrant apply",
        "execute": "vagrant apply \"my-tfplan\"",
        "destroy": "vagrant destroy"
    }
}
```

#### As a globally installed binary:
If you installed this package globally (with `npm i -g` or `yarn global add`), you can simply start using it like a regular command-line program:
```bash
vagrant -v        # show version info
vagrant --help    # show usage info
```
