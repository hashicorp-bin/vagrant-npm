const extractDmg = require("extract-dmg");
 
(async () => {
   console.log( await extractDmg("vagrant_1.8.6.dmg")); // Get contents
    //=> ["a", "b"]
 
//    await extractDmg("vagrant_1.8.6.dmg", "temp"); // Extract and get contents
    //=> ["a", "b"]
})()
