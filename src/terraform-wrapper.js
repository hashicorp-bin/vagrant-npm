#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

const execName = process.platform === 'win32' ? 'vagrant.exe' : './vagrant';
const command = resolve(__dirname, '..', 'tools', execName);
const vagrant = spawn(command, process.argv.slice(2), { cwd: process.cwd() });

vagrant.stdout.pipe(process.stdout);
vagrant.stderr.pipe(process.stderr);
vagrant.on('error', function(err) {
  console.error(`Received an error while executing the Vagrant binary: ${err}`);
  process.exit(1);
});
